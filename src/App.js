import React, {Component} from 'react'
import './App.css'
import HTTP from './utils/HTTP'
import { Badge, Button, Col, Label, Row, Well, ButtonGroup } from 'react-bootstrap'
import Datetime from 'react-datetime'

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            list: [],
            start: null,
            end: null
        }

        this.handleChangeStart = this.handleChangeStart.bind(this)
        this.handleChangeEnd = this.handleChangeEnd.bind(this)
        this.handleFilter = this.handleFilter.bind(this)
        this.handleClear = this.handleClear.bind(this)
    }

    handleChangeStart(date) {
        this.setState({ start: date })
    }

    handleChangeEnd(date) {
        this.setState({ end: date })
    }

    handleFilter() {
        this.getComments({
            start: this.state.start ? this.state.start.format() : null,
            end: this.state.end ? this.state.end.format() : null
        })
            .then((data) => {
                console.log(data)
                this.setState({list: data})
            })
            .catch((error) => {
                console.error(error)
            })
    }

    handleClear() {
        this.setState({ start: null, end: null })
        this.getComments({ start: null, end: null})
            .then((data) => {
                console.log(data)
                this.setState({list: data})
            })
            .catch((error) => {
                console.error(error)
            })
    }


    async getComments(data = {}) {
        const result = await HTTP.post('/', data)
        if (result) {
            return result.data
        }
    }

    componentDidMount() {
        this.getComments()
            .then((data) => {
                console.log(data)
                this.setState({list: data})
            })
            .catch((error) => {
                console.error(error)
            })
    }

    render() {

        const list = this.state.list
        return (
            <div className="App">
                <div className="App-header">
                    <h2>Comments</h2>
                </div>

                <div className="container">
                    <Row style={{ marginBottom: '20px' }}>
                        <Col md={5}>
                            <Label className="control-label">Start</Label>
                            <Datetime
                                selected={this.state.start}
                                onChange={this.handleChangeStart}
                                dateFormat="YYYY-MM-DD"
                                timeFormat="HH:mm:ss"
                                size="md"
                            />
                        </Col>
                        <Col md={5}>
                            <Label className="control-label">End</Label>
                            <Datetime
                                selected={this.state.end}
                                onChange={this.handleChangeEnd}
                                dateFormat="YYYY-MM-DD"
                                timeFormat="HH:mm:ss"
                                size="md"
                            />
                        </Col>
                        <Col md={2}>
                            <ButtonGroup>
                                <Button onClick={this.handleFilter}
                                        style={{marginTop: '20px', fontSize: '20px', padding: '5px 10px'}}>
                                    Filter
                                </Button>
                                <Button onClick={this.handleClear}
                                        style={{marginTop: '20px', fontSize: '20px', padding: '5px 10px'}}>
                                    Clear
                                </Button>
                            </ButtonGroup>
                        </Col>
                    </Row>

                    <div>
                        <Row>
                            {list.length > 0 ? list.map(function (comment) {
                                return (
                                    <Col key={comment.id} md={12}>
                                        <Well>
                                            <p>{comment.text}</p>
                                            <Badge>{comment.date}</Badge>
                                        </Well>
                                    </Col>
                                )
                            }) : null}
                        </Row>
                    </div>
                </div>

            </div>
        )
    }
}

export default App

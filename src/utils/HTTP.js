import axios from 'axios'
import { backendDomen } from "../config"

export default class HTTP {
    static get(url, params) {
        return axios({
            method: 'GET',
            url: backendDomen + url,
            params: params,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function (value) {
            return value
        }).catch(function (reason) {
            if (reason.response.status === 401) {
                window.location.replace('/')
            }
        })
    }

    static post(url, data, params) {
        return axios({
            method: 'POST',
            url: backendDomen + url,
            data: data,
            params: params,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function (value) {
            return value
        }).catch(function (reason) {
            console.log(reason.response)
            return reason.response
        })
    }
}
